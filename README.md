Pasos para la instalacion

1.- Abrir terminal donde desea descargar el proyecto.

2.- Clonar proyecto ejecutando git clone git@gitlab.com:alopez.215.95/api-terminal.git

3.- Entrar al directorio raiz del proyecto.

4.- Ejecutar el comando 'composer install' en su terminal.

5.- Crear el archivo .env a partir de .env.example.

6.- Crear base de datos en mysql con el nombre 'api_terminal'.

7.- Ejecutar el comando 'php artisan key:generate' en su terminal.

8.- Ejecutar el comando 'php artisan migrate --seed' en su terminal.

9.- Ya puede iniciar el servidor


CONSULTAS 
*Ojo: puede cambiar apiterminal.test por localhost:8000

Frecuencia con la que trabaja un bus en la semana por rutas o no:
http://apiterminal.test/api/job-frequencies
http://apiterminal.test/api/job-frequencies?routes=1,2,67

Días del calendario que el usuario posee reservados, los días reservados pueden conseguirse en formato de rango como tambien días sueltos por o sin rutas:
http://apiterminal.test/api/reserved-days?date_ini=2021-12-24&date_end=2021-12-31&user_id=35&is_range=1&routes=56

Si la capacidad de la ruta no está completamente ocupada:
http://apiterminal.test/api/capacity-is-occupied?routes=60,67

Mostrar los días de calendario no disponibles:
http://apiterminal.test/api/days-disabled/2

