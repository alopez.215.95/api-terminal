<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(CalendarsSeeder::class);
        $this->call(CalendarDaysDisabledSeeder::class);
        $this->call(RouteSeeder::class);
        $this->call(UserPlanSeeder::class);
        $this->call(RouteDataSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ReservationSeeder::class);
    }
}
