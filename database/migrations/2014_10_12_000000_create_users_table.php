<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('picture')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->timestamp('last_online')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('new_email')->nullable();
            $table->integer('status');
            $table->integer('first');
            $table->timestamp('last_accept_date')->nullable();
            $table->string('company_contact')->nullable();
            $table->decimal('credits', 8, 2)->nullable();
            $table->integer('first_trip');
            $table->boolean('incomplete_profile');
            $table->boolean('phone_verify');
            $table->string('token_auto_login')->nullable();
            $table->string('user_vertical')->nullable();
            $table->string('language_id')->nullable();
            $table->boolean('no_registered');
            $table->rememberToken()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
