<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('currency_id');
            $table->integer('next_user_plan_id')->nullable();
            $table->timestamp('start_timestamp')->nullable();
            $table->timestamp('end_timestamp')->nullable();
            $table->timestamp('renewal_timestamp')->nullable();
            $table->decimal('renewal_price', 8, 2)->default(0.0);
            $table->boolean('requires_invoice')->default(false);
            $table->integer('status');
            $table->decimal('financiation', 8, 2)->nullable();
            $table->boolean('status_financiation')->default(false);
            $table->string('language')->nullable();
            $table->string('nif')->nullable();
            $table->string('business_name')->nullable();
            $table->boolean('pending_payment')->default(false);
            $table->timestamp('date_max_payment')->nullable();
            $table->timestamp('proxim_start_timestamp')->nullable();
            $table->timestamp('proxim_end_timestamp')->nullable();
            $table->timestamp('proxim_renewal_timestamp')->nullable();
            $table->decimal('proxim_renewal_price', 8, 2)->nullable();
            $table->decimal('credits_return', 8, 2)->default(0.0);
            $table->integer('company_id');
            $table->boolean('cancel_employee');
            $table->boolean('force_renovation');
            $table->timestamp('date_canceled')->nullable();
            $table->decimal('amount_confirm_canceled', 8, 2)->nullable();
            $table->decimal('credit_confirm_canceled', 8, 2)->nullable();
            $table->integer('cost_center_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
