<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('external_id');
            $table->string('external_budget_id');
            $table->string('external_route_id');
            $table->integer('track_id')->nullable();
            $table->string('name')->nullable();
            $table->string('notes')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->string('arrival_address');
            $table->timestamp('arrival_timestamp')->nullable();
            $table->string('departure_address');
            $table->timestamp('departure_timestamp')->nullable();
            $table->integer('capacity')->nullable();
            $table->boolean('confirmed_pax_count')->default(false);
            $table->timestamp('reported_departure_timestamp')->nullable();
            $table->integer('reported_departure_kms')->nullable();
            $table->timestamp('reported_arrival_timestamp')->nullable();
            $table->integer('reported_arrival_kms')->nullable();
            $table->integer('reported_vehicle_plate_number')->nullable();
            $table->integer('status')->nullable();
            $table->json('status_info')->nullable();
            $table->boolean('reprocess_status')->default(false);
            $table->boolean('return')->default(false);
            $table->integer('synchronized_downstream')->nullable();
            $table->integer('synchronized_upstream')->nullable();
            $table->integer('province_id');
            $table->integer('sale_tickets_drivers');
            $table->string('notes_drivers')->nullable();
            $table->text('itinerary_drivers')->nullable();
            $table->string('cost_if_externalized')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
