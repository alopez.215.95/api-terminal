<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/capacity-is-occupied', 'RouteController@index');
Route::get('/calendar', 'CalendarController@index');
Route::get('/days-disabled/{calendar_id}', 'CalendarController@days_disabled');
Route::get('/job-frequencies', 'RouteDataController@job_frequencies');
Route::get('/reserved-days', 'ReservationController@reservedDays');
