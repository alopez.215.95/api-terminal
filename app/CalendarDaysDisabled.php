<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CalendarDaysDisabled extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'calendar_days_disableds';

    public $timestamps = true;

    protected $fillable = [
        'calendar_id',
        'day',
        'enable',
    ];

    public function calendar()
    {
        return $this->belongsTo(Calendar::class);
    }

}
