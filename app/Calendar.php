<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'calendars';

    public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    public function calendarDaysDisabled()
    {
        return $this->hasMany(CalendarDaysDisabled::class, 'calendar_id');
    }

    public function routeData()
    {
        return $this->hasMany(RouteData::class, 'calendar_id');
    }

}
