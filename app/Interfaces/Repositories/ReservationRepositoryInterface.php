<?php

namespace App\Interfaces\Repositories;

interface ReservationRepositoryInterface {

    public function getReservedDays($params);

}
