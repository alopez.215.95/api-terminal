<?php

namespace App\Interfaces\Repositories;

interface CalendarRepositoryInterface {

    public function getDaysDisabled($request, $calendar_id);

}
