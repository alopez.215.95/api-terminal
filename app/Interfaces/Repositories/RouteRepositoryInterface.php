<?php

namespace App\Interfaces\Repositories;

interface RouteRepositoryInterface {

    public function getCapacityIsOccupiedAllOrForRoutes($request);

}
