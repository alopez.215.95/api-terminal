<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'reservations';

    public $timestamps = true;

    protected $fillable = [
        'user_plan_id',
        'route_id',
        'track_id',
        'reservation_start',
        'reservation_end',
        'route_stop_origin_id',
        'route_stop_destination_id',
    ];

    public function user_plan()
    {
        return $this->belongsTo(UserPlan::class, 'user_plan_id');
    }

    public function route()
    {
        return $this->belongsTo(Route::class, 'route_id');
    }

}
