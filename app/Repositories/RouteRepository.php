<?php

namespace App\Repositories;

use App\Interfaces\Repositories\RouteRepositoryInterface;
use App\Route;

class RouteRepository implements RouteRepositoryInterface {

    public function getCapacityIsOccupiedAllOrForRoutes($request)
    {
        if ( isset($request->routes)) {
            $routes = explode(",", $request->routes);
            $data = Route::whereIn('id', $routes)->whereHas('routeData')->get()->reverse();
        } else {
            $data = Route::whereHas('routeData')->get()->reverse();
        }
        return $data;
    }
}
