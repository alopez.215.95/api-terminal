<?php

namespace App\Repositories;

use App\Interfaces\Repositories\ReservationRepositoryInterface;
use App\Reservation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class ReservationRepository implements ReservationRepositoryInterface {

    public function getReservedDays($request)
    {
        if ( isset($request->user_id)) {
            $this->user_id = $request->user_id;
            $reservations = Reservation::with('user_plan')->whereHas('user_plan', function (Builder $query) {
                $query->with('user')->where('user_id', $this->user_id);
            })->get();
        } else {
            $reservations = Reservation::all();
        }

        if (isset($request->is_range) and $request->is_range == 0) {
            if ( isset($request->date_ini, $request->date_end) ) {
                $date_ini = new Carbon($request->date_ini);
                $reservations = $reservations->whereBetween('reservation_start', [$date_ini->startOfDay(), $date_ini->copy()->endOfDay()]);
            }
            if ( isset($request->date_ini) and !isset($request->date_end)) {
                $dt = new Carbon($request->date_ini);
                $reservations = $reservations->whereBetween('reservation_start', [$dt->startOfDay(), $dt->copy()->endOfDay()]);
            }
            if ( isset($request->date_end) and !isset($request->date_ini)) {
                $dt = new Carbon($request->date_end);
                $reservations = $reservations->whereBetween('reservation_start', [$dt->startOfDay(), $dt->copy()->endOfDay()]);
            }
        } else {
            if ( isset($request->date_ini, $request->date_end) ) {
                $date_ini = new Carbon($request->date_ini);
                $date_end = new Carbon($request->date_end);
                $reservations = $reservations->whereBetween('reservation_start', [$date_ini->startOfDay(), $date_end->endOfDay()]);
            }
            if ( isset($request->date_ini) and !isset($request->date_end)) {
                $reservations = $reservations->where('reservation_start', '>=', $request->date_ini);
            }
            if ( isset($request->date_end) and !isset($request->date_ini)) {
                $reservations = $reservations->where('reservation_start', '<=', $request->date_end);
            }
        }

        if ( isset($request->routes)) {
            $routes = explode(",", $request->routes);
            $reservations = $reservations->whereIn('route_id', $routes);
        }

        return $reservations->toArray();
    }
}
