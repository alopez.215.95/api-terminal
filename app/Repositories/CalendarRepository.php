<?php

namespace App\Repositories;

use App\CalendarDaysDisabled;
use App\Interfaces\Repositories\CalendarRepositoryInterface;
use App\Reservation;
use Illuminate\Database\Eloquent\Builder;

class CalendarRepository implements CalendarRepositoryInterface {

    public function getDaysDisabled($request, $calendar_id)
    {
        if ( isset($request->routes)) {
            $this->routes = explode(",", $request->routes);
            $this->calendar_id = $calendar_id;

            $calendar = CalendarDaysDisabled::with('calendar')->whereHas('calendar', function (Builder $query) {
                $query->where('id', $this->calendar_id)->whereHas('routeData', function (Builder $query2) {
                    $query2->whereIn('route_id', $this->routes);
                });
            })->get();
        } else {
            $calendar = CalendarDaysDisabled::with('calendar')->where('calendar_id', $calendar_id)->get()->reverse();

        }

        return $calendar;
    }
}
