<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\CalendarDaysDisabled;
use App\Repositories\CalendarRepository;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    private $calendarRepository;

    public function __construct(CalendarRepository $calendarRepository)
    {
        $this->calendarRepository = $calendarRepository;
    }

    public function index(Request $request)
    {
        $calendar = Calendar::all()->reverse();

        return response()->json($calendar);
    }

    public function days_disabled(Request $request, $calendar_id)
    {
        $calendar = $this->calendarRepository->getDaysDisabled($request, $calendar_id);

        return response()->json($calendar);
    }
}
