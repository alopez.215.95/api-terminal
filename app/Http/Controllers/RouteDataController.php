<?php

namespace App\Http\Controllers;

use App\RouteData;
use Illuminate\Http\Request;

class RouteDataController extends Controller
{
    public function job_frequencies(Request $request)
    {
        if ( isset($request->routes)) {
            $routes = explode(",", $request->routes);
            $data = RouteData::with('route')->whereIn('route_id', $routes)->get()->reverse();
        } else {
            $data = RouteData::with('route')->get()->reverse();
        }
        return response()->json($data);
    }
}
