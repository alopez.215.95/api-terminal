<?php

namespace App\Http\Controllers;

use App\Repositories\RouteRepository;
use App\Route;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    private $RouteRepository;

    public function __construct(RouteRepository $RouteRepository)
    {
        $this->routeRepository = $RouteRepository;
    }

    public function index(Request $request)
    {
        $data = $this->routeRepository->getCapacityIsOccupiedAllOrForRoutes($request);
        return response()->json($data);
    }

}
