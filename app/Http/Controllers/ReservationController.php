<?php

namespace App\Http\Controllers;

use App\Repositories\ReservationRepository;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    private $reservationRepository;

    public function __construct(ReservationRepository $reservationRepository)
    {
        $this->reservationRepository = $reservationRepository;
    }

    public function reservedDays(Request $request)
    {
        $reservations = $this->reservationRepository->getReservedDays($request);

        return response()->json(array_reverse($reservations));
    }
}
