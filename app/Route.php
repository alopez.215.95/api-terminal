<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'routes';

    public $timestamps = true;

    protected $fillable = [
        'external_id',
        'invitation_code',
        'title',
        'start_timestamp',
        'end_timestamp',
    ];

    protected $appends = [
        'free_days',
        'capacity_is_occupied'
    ];

    public function routeData()
    {
        return $this->hasOne(RouteData::class, 'route_id');
    }

    public function getFreeDaysAttribute()
    {
        $freeDays = [];
        if (!$this->routeData->mon) {
            array_push($freeDays, 'mon');
        }
        if (!$this->routeData->tue) {
            array_push($freeDays, 'tue');
        }
        if (!$this->routeData->wed) {
            array_push($freeDays, 'wed');
        }
        if (!$this->routeData->thu) {
            array_push($freeDays, 'thu');
        }
        if (!$this->routeData->fri) {
            array_push($freeDays, 'fri');
        }
        if (!$this->routeData->sat) {
            array_push($freeDays, 'sat');
        }
        if (!$this->routeData->sun) {
            array_push($freeDays, 'sun');
        }
        return $freeDays;
    }

    public function getCapacityIsOccupiedAttribute()
    {
        if (!$this->routeData->sun) {
            return false;
        }
        if (!$this->routeData->mon) {
            return false;
        }
        if (!$this->routeData->tue) {
            return false;
        }
        if (!$this->routeData->wed) {
            return false;
        }
        if (!$this->routeData->thu) {
            return false;
        }
        if (!$this->routeData->fri) {
            return false;
        }
        if (!$this->routeData->sat) {
            return false;
        }
        return true;
    }

}
